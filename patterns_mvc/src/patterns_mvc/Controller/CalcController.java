/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns_mvc.Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import patterns_mvc.GUI.CalcView;
import patterns_mvc.Model.CalcModel;

/**
 *
 * @author Mal
 */
public class CalcController {
    
    private CalcView view;
    private CalcModel model;
    
    public CalcController(CalcView view, CalcModel model) {
        this.view = view;
        this.model = model;
        //add the actionListener
        this.view.getComputeBtn().addActionListener(new BtnListener());
    }
    
    class BtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            int a = Integer.parseInt(view.getjTextField1().getText());
            int b = Integer.parseInt(view.getjTextField2().getText());
            
            String operator = view.getjComboBox1().getSelectedItem().toString();
            
            view.setjLabel1(Integer.toString(model.calc(operator, a, b)));
        }
        
    }
    
}
