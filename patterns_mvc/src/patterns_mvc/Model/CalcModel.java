/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns_mvc.Model;

/**
 *
 * @author Mal
 */
public class CalcModel {
    
    public CalcModel() {
        
    }
    public int calc(String operator, int a, int b) {
        if(operator.equals("+")) {
            return plus(a, b);
        } else if(operator.equals("*")) {
            return multiply(a, b);
        } else if(operator.equals("-")) {
            return minus(a,b);
        } else if(operator.equals("/")) {
            return division(a, b);
        } else {
            return 0;
        }
    }
    
    public int plus(int a, int b) {
        return a + b;
    }
    public int multiply(int a, int b) {
        return a*b;
    }
    public int minus(int a, int b) {
        return a-b;
    }
    public int division(int a, int b) {
        return (int) a/b;
    }
    
}
