/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns_mvc;

import patterns_mvc.Controller.CalcController;
import patterns_mvc.GUI.CalcView;
import patterns_mvc.Model.CalcModel;

/**
 *
 * @author Mal
 */
public class Patterns_mvc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CalcView view = new CalcView();
        CalcModel model = new CalcModel();
        CalcController controller = new CalcController(view, model);
    }
    
}
