/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package launcher;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Pelle
 */
public class CommandLine_OpenApps {

    private Map<String, URL> urlMap = new HashMap();
    private Map<String, File> applicationMap = new HashMap();
    
    public CommandLine_OpenApps() {
    
        fillMap();
    }
    
    private void fillMap() {
        
        try {
            urlMap.put("DR", new URL("http://www.dr.dk/"));
            urlMap.put("Facebook", new URL("https://www.facebook.com/"));
            urlMap.put("Google", new URL("https://www.google.com/"));
            urlMap.put("StackOverflow", new URL("http://stackoverflow.com/"));
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(CommandLine_OpenApps.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<String> getButtonNamesURL() {
    
        return new ArrayList(urlMap.keySet());
    }
    
   private static void openWebpage(URI uri) {
    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop(): null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private static void openWebpage(URL url) {
        try {
            openWebpage(url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    private static void openApllication(File file) {
    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop(): null;
        if (desktop != null && desktop.isSupported(Desktop.Action.OPEN)) {
            try {
                desktop.open(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void addProgram() {
    
        File file;
        
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Choose Application");
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        chooser.setFileFilter(new FileNameExtensionFilter(".exe files", "exe"));

        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
            System.err.println(file.getName().substring(0, file.getName().indexOf('.')));
            applicationMap.put(file.getName().substring(0, file.getName().indexOf('.')), file);
        } else {
            System.out.println("No Selection");
        }
    }
    
    public void addURL(String urlString) {
    
        URL url = null;
            try {
                url = new URL("http://" + urlString);
            } catch (MalformedURLException ex) {
                Logger.getLogger(CommandLine_OpenApps.class.getName()).log(Level.SEVERE, null, ex);
            }
            String urlName = url.getHost();
            String name = urlName.substring(0, urlName.indexOf('.'));
                if(name.length() > 10) {
                
                    name = name.substring(0, 10);
                }
            urlMap.put(name, url);
    }
    
    public ArrayList<String> getButtonNamesApplication() {
    
        return new ArrayList(applicationMap.keySet());
    }
    
    private void runProgram(String programName) {
    
        File file = applicationMap.get(programName);
        openApllication(file);
    }
    
    private void runWebApplication(String webApplicationName) {
    
        openWebpage(urlMap.get(webApplicationName));
    }
    public void run(String input) {
    
        if(applicationMap.containsKey(input)) {
        
            runProgram(input);
        } else
            runWebApplication(input);
    }
    
    public void closeApplication(String programName) {
        try {
                if(urlMap.containsKey(programName)) {
                    Runtime.getRuntime().exec("Taskkill /IM chrome.exe");
                    Runtime.getRuntime().exec("Taskkill /IM iexplore.exe");
                    Runtime.getRuntime().exec("Taskkill /IM safari.exe");
                }
                else
            Runtime.getRuntime().exec("Taskkill /IM " + programName + ".exe");
        } catch (IOException ex) {
            Logger.getLogger(CommandLine_OpenApps.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
