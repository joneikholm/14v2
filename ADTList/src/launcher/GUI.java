/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package launcher;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JToggleButton;

/**
 *
 * @author ande009
 */
public class GUI extends javax.swing.JFrame{

    /**
     * Creates new form GUI
     */
    
    
    public GUI() {
        initComponents();
        setVisible(rootPaneCheckingEnabled);
    }
    
    CommandLine_OpenApps cml = new CommandLine_OpenApps();
    
    
    public void setBtnName(){
        
        
        ArrayList<String> list1 = cml.getButtonNamesURL();
        ArrayList<String> list2 = cml.getButtonNamesApplication();
        
        float size = 28;
        
        int row = 1;
        int colum = 3;
        
        if((list1.size() + list2.size()) > 3){
            row = (((list1.size() + list2.size()) / 3) + 1);
        }
        
        jPanel1.removeAll();
        jPanel1.setLayout(new GridLayout(row, colum));
                
        for(int i = 0; i < list1.size(); i++){          
            JToggleButton jtbtn = new JToggleButton(list1.get(i));
            
            Font font = jtbtn.getFont();
            jtbtn.setFont(font.deriveFont(size));
            
            //Add ItemListener til URL buttons
            jtbtn.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent ev) {

                    if(ev.getStateChange()==ItemEvent.SELECTED){ 
                        System.out.println("button " + jtbtn.getText() + " is selected");
                        cml.run(jtbtn.getText());
                    } 
                    else if(ev.getStateChange()==ItemEvent.DESELECTED){
                        System.out.println("button " + jtbtn.getText() + " is not selected");
                        cml.closeApplication(jtbtn.getText());
                    }
                }
            });  
            
            //Add Mouselistner til URL buttons
            jtbtn.addMouseListener(new MouseListener() {
                public void mouseReleased(MouseEvent e) {
                    
                    if(e.getButton() == MouseEvent.BUTTON1)
                    {
                        System.out.println("Detected  " + jtbtn.getText() + " Mouse Left Click!");
                    }	   
                    else if(e.getButton() == MouseEvent.BUTTON3)
                    {
                        System.out.println("Detected  " + jtbtn.getText() + " Mouse Right Click!");
                        jFrame2.setSize(283, 246);
                        jFrame2.setLocationRelativeTo(null);
                        jFrame2.setVisible(rootPaneCheckingEnabled);
                    }
                }
                public void mouseEntered(MouseEvent e){
                }
                public void mouseExited(MouseEvent e){
                }
                public void mouseClicked(MouseEvent e){
                }
                public void mousePressed(MouseEvent e){
                }
            });
            
            jPanel1.add(jtbtn);      
        }
        
        for(int i = 0; i < list2.size(); i++){          
            JToggleButton jtbtn = new JToggleButton(list2.get(i));
            
            Font font = jtbtn.getFont();
            jtbtn.setFont(font.deriveFont(size));
            
            //Add ItemListener til Applicationer
            jtbtn.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent ev) {

                    if(ev.getStateChange()==ItemEvent.SELECTED){ 
                        System.out.println("button " + jtbtn.getText() + " is selected");
                        cml.run(jtbtn.getText());
                    } 
                    else if(ev.getStateChange()==ItemEvent.DESELECTED){
                        System.out.println("button " + jtbtn.getText() + " is not selected");
                        cml.closeApplication(jtbtn.getText());
                    }
                }
            }); 
            
            //Add Mouselistner til Applicationer
            jtbtn.addMouseListener(new MouseListener() {
               public void mouseReleased(MouseEvent e) {
                    
                    if(e.getButton() == MouseEvent.BUTTON1)
                    {
                        System.out.println("Detected " + jtbtn.getText() + " Mouse Left Click!");
                    }	   
                    else if(e.getButton() == MouseEvent.BUTTON3)
                    {
                        System.out.println("Detected " + jtbtn.getText() + " Mouse Right Click!");
                        jFrame2.setSize(283, 246);
                        jFrame2.setLocationRelativeTo(null);
                        jFrame2.setVisible(rootPaneCheckingEnabled);
                    }
                }
                public void mouseEntered(MouseEvent e){
                }
                public void mouseExited(MouseEvent e){
                }
                public void mouseClicked(MouseEvent e){
                }
                public void mousePressed(MouseEvent e){
                }
            });
            
            jPanel1.add(jtbtn);   
        }
        
        jPanel1.repaint();
        this.pack();
    }
       
    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jFrame1 = new javax.swing.JFrame();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jFrame2 = new javax.swing.JFrame();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        jButton3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton3.setText("Add");
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton3MouseReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("URL");

        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jFrame1Layout.createSequentialGroup()
                .addGap(134, 134, 134)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jFrame1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jFrame1Layout.createSequentialGroup()
                .addGap(121, 121, 121)
                .addComponent(jButton3))
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jFrame1Layout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(jButton3))
        );

        jFrame2.setMinimumSize(new java.awt.Dimension(283, 215));
        jFrame2.getContentPane().setLayout(new java.awt.GridBagLayout());

        jButton4.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton4.setText("Close");
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton4MouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 97, 20, 0);
        jFrame2.getContentPane().add(jButton4, gridBagConstraints);

        jButton5.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton5.setText("Edit");
        jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton5MouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(38, 106, 0, 0);
        jFrame2.getContentPane().add(jButton5, gridBagConstraints);

        jButton6.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton6.setText("Remove");
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton6MouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 85, 0, 0);
        jFrame2.getContentPane().add(jButton6, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.ipadx = 282;
        gridBagConstraints.ipady = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(18, 0, 0, 0);
        jFrame2.getContentPane().add(jSeparator2, gridBagConstraints);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(730, 600));

        jPanel1.setPreferredSize(new java.awt.Dimension(500, 500));
        jPanel1.setLayout(new java.awt.GridLayout(1, 3));

        jPanel2.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 729;
        gridBagConstraints.ipady = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(38, 0, 0, 0);
        jPanel2.add(jSeparator1, gridBagConstraints);

        jButton1.setText("Add App");
        jButton1.setMaximumSize(new java.awt.Dimension(75, 25));
        jButton1.setMinimumSize(new java.awt.Dimension(75, 25));
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton1MouseReleased(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 93;
        gridBagConstraints.ipady = 13;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 149, 0, 0);
        jPanel2.add(jButton1, gridBagConstraints);

        jButton2.setText("Add URL");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton2MouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 97;
        gridBagConstraints.ipady = 17;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 80, 36, 0);
        jPanel2.add(jButton2, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseReleased
        // TODO add your handling code here:
        jFrame1.setSize(326, 277);
        jFrame1.setLocationRelativeTo(null);
        jFrame1.setVisible(rootPaneCheckingEnabled);
            
    }//GEN-LAST:event_jButton2MouseReleased

    private void jButton3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseReleased
        // TODO add your handling code here:
        String url = jTextField1.getText();
        cml.addURL(url);
        
        setBtnName();
        
        jFrame1.dispose();
        
    }//GEN-LAST:event_jButton3MouseReleased

    private void jButton1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseReleased
        // TODO add your handling code here:
        
        cml.addProgram();
        setBtnName();
    }//GEN-LAST:event_jButton1MouseReleased

    private void jButton4MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseReleased
        // TODO add your handling code here:
        jFrame2.dispose();
    }//GEN-LAST:event_jButton4MouseReleased

    private void jButton6MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseReleased
        // TODO add your handling code here:
        //Remove app/url metode
        
    }//GEN-LAST:event_jButton6MouseReleased

    private void jButton5MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton5MouseReleased
        // TODO add your handling code here:
        //Edit app/url metode
        
    }//GEN-LAST:event_jButton5MouseReleased
    
    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JFrame jFrame2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables


    
}
