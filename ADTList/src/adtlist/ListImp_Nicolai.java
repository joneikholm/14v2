/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtlist;

import java.util.Arrays;

/**
 * List implementation DAT14V2.
 * 
 * @author Nicolai Prenslev
 * @param <T>
 */
public class ListImp_Nicolai<T> implements TheInterface<T>
{
    public Object[] array;
    private int size;

    /**
     * Constructor for list.
     */
    public ListImp_Nicolai()
    {
        array = new Object[0];
        size = 0;
    }
    
    /**
     * Removes a single object from the list, at the index.
     * @param index
     */
    @Override
    public void remove(int index)
    {
        if(index > size)
        {
            System.out.println("Index out of bounds! Size: "+size+". Index tried: "+index);
        }
        else
        {
            int moved = size - index - 1;

            if(moved > 0)
            {
            System.arraycopy(array, index+1, array, index, moved);
            array[--size] = null;
            }
            else
            {
                array = new Object[0];
                size--;
            }
        }
    }

    /**
     * Returns the object from the list, at the index.
     * @param index
     * @return
     */
    @Override
    public T get(int index)
    {
        return (T) array[index];
    }

    /**
     * Adds a new object to the end of the list.
     * @param t
     */
    @Override
    public void add(T t)
    {
        sizeCheck();
        
        array[size++] = t;
    }

    /**
     * Will check if list contains a specified object and return a boolean.
     * @param t
     * @return
     */
    @Override
    public boolean contains(T t)
    {
        for(int i = 0; i < size; i++)
        {
            if(array[i] == t || array[i].equals(t))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Will return the size of the list.
     * @return
     */
    @Override
    public int size()
    {
        return size;
    }
    
    /**
     * Will return a string representation of the list.
     * @return
     */
    @Override
    public String toString()
    {
        return Arrays.toString(array);
    }
    
    /**
     * Will remove all objects in list.
     */
    public void removeAll()
    {
        array = new Object[0];
    }
    
    private void sizeCheck()
    {
        if(size == 0)
        {
            array = new Object[1024];
        }
        else if (size == array.length)
        {
            array = Arrays.copyOf(array, size*2);
        }
    }
    
}
