package adtlist;

import java.util.Arrays;

public class ListImp_Daniel<T> implements TheInterface<T> {

	private Object[] list;
	private int size;
	
	public ListImp_Daniel() {
		list = new Object[10];
		size = 0;
	}

	@Override
	public void remove(int index) {
		for(int i = index; i < size; i++) {
			list[i] = list[i+1];
		}
		size--;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get(int index) {		
		return (T) list[index];
	}

	@Override
	public void add(T t) {
		if(size < list.length) {
			list[size] = t;
		} else {
			list = Arrays.copyOf(list, list.length * 2);
			list[size] = t;
		}
		size++;
	}

	@Override
	public boolean contains(T t) {
		for(int i = 0; i < size; i++) {
			if(list[i] == t) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int size() {
		return size;
	}

}
