package adtlist;

/**
 *
 * @author Mal
 */
public class ListImp_Malthe<T> implements TheInterface<T> {
    private ListNode front; //fronten 1ste node i listen
   
    
    public ListImp_Malthe() {
        //this.front = null;
    }
    
    @Override
    public void remove(int index) {
        //if remove at 0, move next in list to front
        if(index == 0) {
            this.front = front.next;
        } else {
            ListNode current = nodeAt(index);
            //Flyt pointeren en mere(.next) for at fjerne
            current.next = current.next.next;
        }
    }

    @Override
    public T get(int index) {
        return valueAt(index);
    }

    @Override
    public void add(T t) {
        //if list is empty add a new note with the value (at front) 
        if(front == null) { 
            this.front = new ListNode(t);
        // else if list is not empty, loop over, and when at the end,
        //Make a new node to and add it to the end.
        } else {    
            ListNode current = this.front;
            while(current != null) {
                current = current.next;
                current.next = new ListNode(t);
            }
            
        }
        
    }
    //loop over the list to check if it contains the object
    @Override
    public boolean contains(Object t) {
        if(this.front != null) {
            ListNode current = this.front;
            while(current != null) {
                current = current.next;
                if(current.getData().equals(t)) {
                    return true;
                }
            }
        } else {
            return false;
            
        }
        return false;
    }

    @Override
    public int size() {
        int count = 0;
        ListNode current = front;
        while(current != null) {
            current = current.next;
            count++;
        }
        return count;
    }
    private ListNode nodeAt(int Index) {
        ListNode current = front;
        for(int i = 0; i <= Index; i++) {
            current = current.next;
        }
        return current;
    }   
    private T valueAt(int Index) {
        return (T) nodeAt(Index).getData();
    }
    @Override
    public String toString() {
        if(this.front == null) {
            return "[]";
        } else {
            String list = "["+ front.data;
            ListNode current = this.front.next;
            while(current!= null) {
            current = current.next;
            list += ", " + current.data;
            
            }
            list += "]";
            return list;
        }
        
    }
    // inner class
    class ListNode<T> {
        //pointer to the stored Object.
        public T data;
        //pointer to next Node
        public ListNode next;
        //constructs a new node with no reference to next node and no pointer to any data
        public ListNode() {
            this(null, null);
        }
        //constructor with data, but no pointer
        public ListNode(T data) {
            this(data, null);
        }
        //constructor with pointer
        public ListNode(T data, ListNode next) {
            this.data = data;
            this.next = next;
            
        }
        public T getData() {
            return this.data;
        }
        public String toString() {
            return this.data.toString();
        }
    }
        
    
}
