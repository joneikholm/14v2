package adtlist;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Tester {
	TheInterface theInterface;
	private static List<TheInterface> lists = getTheInterfaceListOfLists();
        
	public static void main(String[] args) {
		List<TheInterface> list = getTheInterfaceListOfLists();
		for(int i = 0; i <= 3000000; i++){
			list.get(0).add(i);
		}
//		System.out.println(list.get(0).size());
//		System.out.println(list.get(0).contains(345678));
//		System.out.println(list.get(0).get(345632));
		
		benchmark(lists);
	}
        
	public static List<TheInterface> getTheInterfaceListOfLists() {
        Class<?> clazz;
        String workingDir = System.getProperty("user.dir");
//        System.out.println("working dir: "+workingDir);
        File[] files = new File(workingDir + "/ADTList/src/adtlist").listFiles();
        //check if OS is Windows
        if(System.getProperty("os.name").startsWith("Windows")) {
            workingDir = System.getProperty("user.dir");
            files = new File(workingDir + "\\src\\adtlist").listFiles();
            
            if(files == null) {
            	files = new File(workingDir + "\\ADTList\\src\\adtlist").listFiles();
            }
        }

        List<TheInterface> ninjaList = new ArrayList();
        String name = "";
        for(File file : files)
        {
            if(file.getName().startsWith("ListImp_"))
            {
               name = file.getName().substring(0, file.getName().length()-5);
                try {

                    clazz = Class.forName("adtlist."+name);
                    Object object = clazz.newInstance();
                    TheInterface list = (TheInterface)object;
                    ninjaList.add(list);

                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }           
        }   
        return ninjaList; 
        }

	/**
	 * Used to test each custom ADTList created by students in Dat14v2
	 * @param <T>
	 * @param lists - <i>ArrayList</i> containing single, empty instantized lists of each student's individual list to be tried before time test.<br>
	 * Doesn't break if any error is encountered
	 * @return returns <i>false</i> if there is any sort of error with any list<br>returns <i>true</i> if every list works properly
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> void benchmark(List<TheInterface> lists) {
		
		boolean errorFound = true;
		
		for (TheInterface list : lists) {
			
			errorFound = false;
			
			System.out.println("Checking list: " + list.getClass() + "\n");
			
			TestObject test1 = new TestObject("Test object 1");
			TestObject test2 = new TestObject("Test object 2");
			TestObject test3 = new TestObject("Test object 3");
			TestObject test4 = new TestObject("Test object 4");
			TestObject test5 = new TestObject("Test object 5");
			
			// Check if add() works properly
			try {
				list.add(test1);
				list.add(test2);
				list.add(test3);
				list.add(test4);
				
				for(int i = 0; i < 10000; i++) {
					list.add(new TestObject("HugeTest" + i));
				}
				
				System.out.println("  - Method add() working");
			} catch (Exception e1) {
				errorFound = true;
				System.out.println("  ! Method add() not working");
			}
			
			// Check if get() works properly
			try {
				if(((TestObject) list.get(3)).getName().equalsIgnoreCase("Test Object 4")) {
					System.out.println("  - Method get() working");
				} else {
					errorFound = true;
					System.out.println("  ! Method get() not working");
				}
			} catch (Exception e) {
				errorFound = true;
				System.out.println("  ! Method get() not working");
			}
			
			// Check if contains() works properly
			try {
				if(!list.contains(test5) && list.contains(test2)) {
					System.out.println("  - Method contains() working");
				} else {
					errorFound = true;
					System.out.println("  ! Method contains() not working");
				}
			} catch (Exception e) {
				errorFound = true;
				System.out.println("  ! Method contains() not working");
			}
			
			// Check if size() and remove() works properly
			try {
				if(list.size() == 10004) {
					list.remove(1);
					
					if(list.size() == 10003) {
						System.out.println("  - Method size() working");
					} else {
						errorFound = true;
						System.out.println("  ! Method size() not working");
					}
					
					if(!list.contains(test2)) {
						System.out.println("  - Method remove() working");
					} else {
						errorFound = true;
						System.out.println("  ! Method remove() not working");
					}
					
				} else {
					errorFound = true;
					System.out.println("  ! Method size() not working");
					
					list.remove(1);
					
					if(!list.contains(test2)) {
						System.out.println("  - Method remove() working");
					} else {
						errorFound = true;
						System.out.println("  ! Method remove() not working");
					}
				}
			} catch (Exception e) {
				errorFound = true;
				System.out.println("  ! Method remove() not working");
			}
			System.out.println("");
			
			if(!errorFound) {
				// Run time test
			}
		}
	}
}
