package adtlist;

public class TestObject {
	String name;
	
	public TestObject(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
