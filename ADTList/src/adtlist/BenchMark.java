package adtlist;


import adtlist.TheInterface;

public class BenchMark<T> {

	//Variabler
	long timeStartAdd;
	long timeEndAdd;
	
	long timeStartRemove;
	long timeEndRemove;
	
	long timeStartRemove2;
	long timeEndRemove2;
	
	long timeStartGet;
	long timeEndGet;
	
	
	//Metoder
	
	public String timeTest(TheInterface<String> obj, int i)
	{
		
		String svar = "The amount of objects we test with: " + i + "\n";
		//Tester tiden for add operationen for i add kald.
		timeStartAdd = System.currentTimeMillis();
		
		for(int x = 0; x < i; x++)
		{		
			obj.add("hej");
		}
		
		timeEndAdd = System.currentTimeMillis();
		svar = svar + "Method add(value) took: " + (timeEndAdd - timeStartAdd) + "ms\n";
		
		//Tester tiden for remove operationen for i remove kald fra bagenden.
		timeStartRemove = System.currentTimeMillis();
		
		for(int x = obj.size(); 0 <= x; x--){
			
			obj.remove(x);
		}
		
		timeEndRemove = System.currentTimeMillis();
		svar = svar + "Method remove(index) all from the tail took: " + (timeEndRemove - timeStartRemove) + "ms\n";
		
		//Tester tiden for remove operationen for i remove kald midt i listen.
		for(int x = 0; x < i; x++)
		{		
			obj.add("hej");
		}
		
		timeStartRemove2 = System.currentTimeMillis();
		
		for(int x = obj.size(); 0 <= x; x--){
			if(!(obj.size() == 1))
			{
				obj.remove(1);
			}
			else obj.remove(0);
			
		}
		
		timeEndRemove2 = System.currentTimeMillis();
		svar = svar + "Method remove(index) all from inside the list took: " + (timeEndRemove2 - timeStartRemove2) + "ms\n";
		
		return svar;
	}
	
}
