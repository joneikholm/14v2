/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtlist;

import java.lang.reflect.Array;

/**
 *
 * @author Eric
 * @param <T>
 */
public class ListImp_Eric<T> implements TheInterface<T> {
    //Fields
    private Object[] listArr = new Object[100];
    private int size = 0;
    private final double INCREMENTAL_FACTOR; //This field will be set to a numeric value of 1.5 unless another value is specified in the constructor.
    
    //Methods (Constructors)
    public ListImp_Eric() {
        this.INCREMENTAL_FACTOR = 1.5;
    }
    
    public ListImp_Eric(double f) {
        this.INCREMENTAL_FACTOR = f;
    }
    
    //Methods
    @Override
    public void add(T t) {
        if(size < Array.getLength(listArr)) { //Hvis der er plads i arrayet...
            listArr[size] = t;
            size++;
        } else {
            int newSize = (int)(Array.getLength(listArr) * INCREMENTAL_FACTOR);
            Object[] tempArr = new Object[newSize]; //Laver et nyt og større array.
            
            for (int i = 0; i < size; i++) { //Ryk værdierne fra det gamle array, over i det nye og større array.
                tempArr[i] = listArr[i];
            }
            
            listArr = tempArr;
            add(t);
        }
    }
    
    @Override
    public int size() {
        return size;
    }
    
    @Override
    public T get(int index){
        if ((index >= 0) && (index <= size - 1)) {
            return (T)listArr[index];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    @Override
    public void remove(int index) {
        if (index == size - 1) {
            listArr[size - 1] = null;
            size--;
        } else if ((index >= 0) && (index < size - 1)) {
            for(int i = index; i < size - 2; i++) { //iterer op til det næstsidste element i listen
                listArr[i] = listArr[i+1];
            }
            listArr[size - 1] = null;
            size--;
        } else if ((index < 0) || (index >= size)) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    @Override
    public boolean contains(T t) {
        boolean result = false;
        
        for (int i = 0; i < size; i++) {
            if (listArr[i].toString().equals(t.toString())) {
                result = true;
                break;
            }
        }
        
        return result;
    }
    
    /**
     * You should be using size() instead, as it is more effective.
     * @return 
     */
    public int countSize() {

        if(Array.getLength(listArr) != 0){
            int counter = 0;
            
            for (Object obj : listArr) {
                if (obj != null) {
                    counter++;
                } else {
                    break;
                }
            }
            
            return counter;
        }else{
            return 0;
        }
    }
}
