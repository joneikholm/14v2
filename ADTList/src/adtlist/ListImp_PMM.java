package adtlist;

public class ListImp_PMM<T> implements TheInterface<T>{
    
    private Node head;
    private int listCount;
    
    public ListImp_PMM()
    {
        this.head = new Node(null);
        listCount = 0;
    }
    @Override
    public void remove(int index) {
        if(index > listCount || listCount < 0)
            throw new IllegalArgumentException("index out of bounds");    
        
        Node currentNode = head;
        for(int i = 1; i < index; i++)
        {
            currentNode = currentNode.getNext();
        }
            currentNode.setNext(currentNode.getNext().getNext());
            listCount--;
    }

    @Override
    public T get(int index) {
  
        if(index > listCount || listCount < 0)
            throw new IllegalArgumentException("HOUNDS TO THE DOGS");
        
        Node currentNode = head;
        for(int i = 0; i < index; i++)
        {
           if(currentNode.getNext() == null)
               return null;
                       
           currentNode = currentNode.getNext();
        }
        return (T) currentNode.getData();
    }

    @Override
    public void add(T t) {
       Node tempNode = new Node(t);
       Node currentNode = head;
       if(currentNode.data == null) {
           currentNode.data = t;
           listCount++;
       }
       else {
       while(currentNode.getNext() !=null)
       {
           currentNode = currentNode.getNext();
       }
       currentNode.setNext(tempNode);
       listCount++;
       }
    }

    @Override
    public boolean contains(T t) {
        
        Node currentNode = head;
        
        for(int i = 0; i <= listCount; i++)
        {
            if(currentNode.getData().equals(t))
            return true;
            
            if(currentNode.getNext() != null) {
            
                currentNode = currentNode.getNext();
            }
            
        }
        
        return false;
    }

    @Override
    public int size() {
        return listCount;
    }
    
    @Override
    public String toString()
    {
     
        Node currentNode = head;
        String output = "";
        while(currentNode!=null)
        {
            currentNode = currentNode.getNext();
            output += currentNode.getData() + ", ";
        }
        return output;
    }
    
}

class Node<T>
{
   Node next;
   T data;
   
   public Node(T data)
   {
       this.data = data;
       next = null;
   }
   public Node(T data, Node nextNode)
   {
       this.data = data;
       next = nextNode;
   }
   
   public T getData()
   {
       return data;
   }
   
   public Node getNext()
   {
       return next;
   }
   
   public void setNext(Node nextNode)
   {
       this.next = nextNode;
   }
}
