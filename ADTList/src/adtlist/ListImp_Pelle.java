/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtlist;

import java.util.Arrays;

/**
 *
 * @author Delonge
 */
public class ListImp_Pelle<T> implements TheInterface<T> {

    private Object[] array;
    private int size;
    
    public ListImp_Pelle() {
    
        array = new Object[1000];
        size = 0;
    }
    @Override
    public void remove(int index) {
        
        checkIndexOutOfBounds(index);
        
        for(int i = index; i < size; i++) {
        
            array[i] = array[i+1];
        }
        size--;
    }

    @Override
    public T get(int index) {
        
        return (T)array[index];
    }

    @Override
    public void add(T t) {
        
        if(size == array.length)
        checkSize();
        
        array[size] = t;
        size++;
    }

    @Override
    public boolean contains(T t) {

       for(int i = 0; i < size; i++ ) {
       
           if(array[i].equals(t)) {
           
               return true;
           }
       }
       return false;
    }

    @Override
    public int size() {

        return size;
    }
    
    private void checkSize() {
    
        array = Arrays.copyOf(array, size*2);
    }
    private void checkIndexOutOfBounds(int index) {
    
            if(index > size || index < 0) {
                throw new ArrayIndexOutOfBoundsException();
        }
    }
    
}
