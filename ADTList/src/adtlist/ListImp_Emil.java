//import java.util.Arrays;
/*
 * Lav en linked list til at holde styr p� tomme pladser i arrayet, s� kan man bare s�tte felter til Null
 * For at holde styr p� det med get() skal der en metode som holder �je og t�ller med felter under size.
 * 
 */
package adtlist;

public class ListImp_Emil<T> implements TheInterface<T>
{

	//Variabler
	int power = 5;
	int size = 0;
	Object[] array = new Object[(int) Math.pow(2, power)];
	
	
	//metoder
	@Override
	public void remove(int index) 
	{
		if(index < size)
		{	
//			//Laver et nyt array hvor det vaerdien index er vaek.
//			Object[] newArray = new Object[(int) Math.pow(2, power)];
//		
//			for(int i = 0; i < index; i++)
//			{
//				newArray[i] = array[i];
//			}
//			
//			//Arrays.fill(arg0, arg1);
//			for(int i = index; i <= size-1; i++)
//			{
//				newArray[i] = array[i+1];
//			}
//			array = newArray;
			
			for(int i = index; i < size; i++)
			{
				array[i] = array[i+1];
			}
			size--;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get(int index) 
	{
		return (T) array[index];
	}

	@Override
	public void add(T t)
	{
		
		/* If(isArrayFull)
		 * Tester om arrayet er fuldt, hvis det er saa laver vi et et nyt 
		 * og overfoere vaerdierne fra det gamle til det og aendre referencen.
		 */
		if(isArrayFull())
		{
			power++;
			Object[] newArray = new Object[(int) Math.pow(2, power)];
			for(int i = 0; i < array.length; i++)
			{
				newArray[i] = array[i];
			}
			
			array = newArray;
			array[size] = t;
			size++;			
		} 
		else 
		{
			array[size] = t;
			size++;
		}
	}

	@Override
	public boolean contains(T t) 
	{
		boolean flag = false;
		
		for(int i = 0; i < size; i++)
		{
			if(array[i] == t) flag = true;
		}
		
		return flag;
	}

	@Override
	public int size() 
	{
		return size;
	}
	
	private boolean isArrayFull()
	{
		return size==array.length-1;
	}
}
