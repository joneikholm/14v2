
package adtlist;

public interface TheInterface<T> {
    
    public void remove(int index);
    
    public T get(int index);
    
    public void add(T t);
    
    public boolean contains(T t);
    
    public int size();
    
}