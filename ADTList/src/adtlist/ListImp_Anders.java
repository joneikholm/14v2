/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtlist;

/**
 *
 * @author ande009
 */
public class ListImp_Anders<T> implements TheInterface<T>{

    private int size;
    private Object[] arr;
    private Object[] arr2;

    public ListImp_Anders() {
        this.arr = new Object[64];  
    }
    
    @Override
    public void remove(int index) {
        
        if(index <= size){
            arr[index] = null;

            for(int i = index; i < arr.length -1; i++){

                if(index != size)
                arr[i] = arr[i +1];
                /*else 
                arr[i] = null;*/    
            }
            size--;  
        } 
    }

    @Override
    public T get(int index) {
        return (T)arr[index];   
    }

    public void add(T t) {   
        
        if(arr.length == size){
            
            //arr = Arrays.copyOf(arr, arr.length*2);
            
            arr2 = arr.clone();   
            arr = new Object[arr2.length * 2];
            for(int i = 0; arr2.length > i; i++){
                this.arr[i] = this.arr2[i];  
            }
            
            
            //System.out.println(arr.length);
        }     
        this.arr[size] = t;
        size++;   
    }

    @Override
    public boolean contains(T t) {
    boolean check = false;
    
        for(int i = 0; i < size; i++){   
            if(t.equals(arr[i]))
                check = true;
        }
        return check;
    }

    @Override
    public int size() { 
        return size;
    }   
    
}
